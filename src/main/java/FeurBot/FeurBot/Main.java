package FeurBot.FeurBot;

import java.io.FileInputStream;
import java.util.Properties;

import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
//import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

public class Main extends ListenerAdapter {
    public static void main(String[] args) {
        
        Properties botTokenProperty = new Properties();
        try {
            botTokenProperty.load(new FileInputStream("./app.properties"));
            String token = botTokenProperty.getProperty("token");
            
            JDABuilder.createDefault(token)
                    .addEventListeners(new MessageEvent())
                    .disableCache(CacheFlag.ACTIVITY).setActivity(Activity.listening("to your messages..."))
                    .enableIntents(GatewayIntent.MESSAGE_CONTENT)
                    .build().awaitReady();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Token or properties file isn't correct! Please re-launch the bot with the correct information.");
            System.exit(1);
        }
    }
}

class MessageEvent extends ListenerAdapter {
	@Override
    public void onMessageReceived(MessageReceivedEvent event) {
		if (event.getAuthor().isBot())
            return;
		
		String message = event.getMessage().getContentRaw();
		String trimmedSentence = message.replaceAll("[\\p{Punct}\\s]+$", "");
		String[] words = trimmedSentence.split("\\s+");
		String lastWord = words[words.length -1].toLowerCase();
		if (lastWord.matches(".*quoi$")) {
			event.getMessage().reply("https://tenor.com/view/feur-meme-gif-24407942").queue();
		}
	}
}
